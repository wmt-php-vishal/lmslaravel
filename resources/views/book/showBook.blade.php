@include('html')


<form action="">
    <h3>Show Book</h3>
</form>

<form action="" method="GET" class="mt-5" >

    <table class="table table-bordered table-dark">
        <thead>
        <tr>

            <th scope="col">Title</th>
            <th scope="col">Author Name</th>
            <th scope="col">Pages</th>
            <th scope="col">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>

            <td>{{$book->title}}</td>
            <td> {{$book->author->fname ." " . $book->author->lname}}</td>
            <td>{{$book->pages}}</td>
            <td>{{$book->book_description}}</td>

        </tr>

        </tbody>
    </table>

</form>
