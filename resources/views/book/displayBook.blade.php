@extends('html')
@section('js')
{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--}}

    <script type="text/javascript" src="{{ asset('assets/js/plugins/buttons/ladda.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/components_buttons.js') }}"></script>




@endsection
@section('title' , 'Display Book')


{{--@foreach($books as $book)--}}

{{--@endforeach--}}

@section('content')

    <table class="table datatable-basic mt-5">
        <thead>
        <tr>
            <th scope="col">Title</th>
            <th scope="col">Author Name</th>
            <th scope="col">Pages</th>
            <th scope="col">Description</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach($books as $book)
            <tr>
                <input type="text" id="$book->id" hidden value="{{$book->id}}">
{{--                <label for=""  id="book_id" >{{$book->id}}</label>--}}
                <td>{{ $book->title }}</td>
                <td>{{ $book->author->fname." ".$book->author->lname }}</td>
                <td>{{ $book->pages }}</td>
                <td>{{ $book->book_description }}</td>
                <td>
                    {{--                    <form action="/book/{{$book->id}}" method="post">--}}
                    {{--                        @csrf--}}
                    {{--                        @method('DELETE')--}}
                    {{--                        <a href="/book/{{$book->id}}/edit"><i class="fas fa-edit mx-2"> </i> </a>--}}
                    {{--                        <button class="border-0 ml-2" type="submit" name="delete"><i class="fas fa-trash-alt"></i></button>--}}
                    {{--                        <a href="/book/{{$book->id}}"><i class="far ml-2 fa-eye"></i></a>--}}
                    {{--                    </form>--}}

                    <a href="/book/{{$book->id}}/edit"><i class="fas fa-edit mx-2"> </i> </a>
                    <a href=""  class="border-0 ml-2 deleteBtn" data-id="{{ $book->id }}" name="delete"><i class="fas fa-trash  mx-2"> </i> </a>
{{--                    <button class="border-0 ml-2 deleteBtn" data-id="{{ $book->id }}" name="delete"><i class="fas fa-trash-alt"></i></button>--}}
                    <a href="/book/{{$book->id}}"> <i class="far ml-2 fa-eye"></i></a>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <b> {{ $books->count() . " Records Found"}}</b>
    <form action="/book/create" class="text-center">
        @csrf
        <button type="submit" class=" btn btn-primary btn-lg text-center">Add New Book</button>
    </form>

@endsection

<script src="{{asset('js/jquery.js')}}"></script>
<script type="text/javascript" >

    $(document).ready(function(){
        $(".deleteBtn").click(function(){
            // alert('hello');
            let bookId = $(this).attr('data-id');

            var url = "{{ route('book.destroy' , ['book' => ':bookId'])}}";
             url = url.replace(':bookId', bookId);
console.log('------',url);
            console.log(bookId);
            $.ajax({
                type:'POST',
                data:{_method: 'delete',_token : '{{ csrf_token() }}' },
                // method: "DELETE",
                {{--                    url: "{{ url('/book/{$book->id}') }}",--}}
                url: url,

                success: function() {
                    window.location.href = "{{ route('book.index')}}";


                }


            });
        });
    });
</script>
