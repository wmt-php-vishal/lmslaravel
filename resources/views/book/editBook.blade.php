@extends('html')

@section('js')
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_selectbox.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/selectboxit.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js')}}"></script>

@endsection

@section('content')


<div class="row">
            <div class="col-md-8">

                <!-- Basic legend -->
                <form class="form-horizontal" action="/book/{{$book->id}}" method="POST" id="">
                @csrf
                @method('PUT')
                    <div class="panel panel-flat">

                        <div class="panel-body">
                            <fieldset>
                                <legend class="text-semibold">Update Book</legend>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Titile:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="title" value="{{$book->title}}">
                                    </div>
                                    <div style="color: red"> @error('title')
                                        {{$message}}
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Pages:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="pages" value="{{$book->pages}}">
                                    </div>
                                    <div style="color: red"> @error('pages')
                                        {{$message}}
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="" class="col-lg-3 control-label">Author</label>
                                    <div class="col-lg-9">

                                            <select name="select" class="selectbox form-control">
                                                <!-- <option value="#">Select Author</option> -->
                                                @foreach ($authors as  $author)
                                                    <option value="{{ $author->id }}" {{$book->author_id == $author->id ? "selected" : "" }}>{{ $author->fname}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <div style="color: red"> @error('select')
                                        {{$message}}
                                        @enderror
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Book Description:</label>
                                    <div class="col-lg-9">
                                        <textarea rows="5" cols="5" name="description" class="form-control" >{{$book->book_description}}</textarea>
                                    </div>
                                    <div style="color: red"> @error('description')
                                        {{$message}}
                                        @enderror
                                    </div>
                                </div>
                            </fieldset>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Update Book <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /basic legend -->

            </div>
        </div>

<!--
<form action="/book/{{$book->id}}" method="POST">
    @csrf
    @method('PATCH')


    <div class="container mt-5">
        <h2>Insert Book Details</h2>
        <div class="form-group row mt-5 ">
            <label for="" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="title" placeholder="Title" value="{{$book->title}}">
            </div>
            <div style="color: red"> @error('title')
                {{$message}}
                @enderror
            </div>
        </div>



        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Pages</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" name="pages" placeholder="Pages" value="{{$book->pages}}">
            </div>
            <div style="color: red"> @error('pages')
                {{$message}}
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Author</label>
            <div class="col-sm-4">

                <select name="select" class="form-control">
                    <option value="#">Select Author</option>
                    @foreach ($authors as  $author)
                        <option value="{{ $author->id }}" {{$book->author_id == $author->id ? "selected" : "" }} >{{ $author->fname}}</option>
                    @endforeach
                </select>


            </div>
            <div style="color: red"> @error('select')
                {{$message}}
                @enderror
            </div>
        </div>



        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-4">
        <textarea name="description" class="form-control" id="" cols="30" rows="8" placeholder="Description"
        >{{$book->book_description}}</textarea>
            </div>
            <div style="color: red"> @error('description')
                {{$message}}
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <div class="col-sm-10 ">
                <button type="submit" class="btn btn-primary">Add Book</button>
            </div>
        </div>


    </div>

</form> -->
@endsection
