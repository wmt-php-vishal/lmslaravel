@extends('html')

@section('js')
  <!-- Theme JS files -->
  <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>

  <script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/pages/form_selectbox.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/selectboxit.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/ui/ripple.min.js')}}"></script>
  <!-- <script type="text/javascript" src="assets/js/core/app.js"></script> -->

  <!-- <script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script> -->

    <!-- <script type="text/javascript" src="{{asset('assets/js/pages/form_layouts.js')}}"></script> -->

	<!-- <script type="text/javascript" src="assets/js/core/libraries/jquery_ui/core.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/selectboxit.min.js"></script>
	<script type="text/javascript" src="assets/js/pages/form_selectbox.js"></script> -->
    <!-- /theme JS files -->


@endsection

@section('content')

<div class="row">
            <div class="col-md-8">

                <!-- Basic legend -->
                <form class="form-horizontal" action="/book" method="POST" id="">
                @csrf
                    <div class="panel panel-flat">

                        <div class="panel-body">
                            <fieldset>
                                <legend class="text-semibold">Add New Book</legend>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Titile:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="title" placeholder="Enter Title">
                                    <div style="color: red"> @error('title')
                                        {{$message}}
                                        @enderror
                                    </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Pages:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="pages" placeholder="Pages of book">
                                    <div style="color: red"> @error('pages')
                                        {{$message}}
                                        @enderror
                                    </div>
                                    </div>

                                </div>


                                <!-- Default select -->
									<!-- <div class="form-group">
										<label>Default select</label>
										<select class="selectbox">
											<option value="AK">Alaska</option>
											<option value="HI">Hawaii</option>
											<option value="CA">California</option>
											<option value="NV">Nevada</option>
											<option value="OR">Oregon</option>
										</select>
									</div> -->
									<!-- /default select -->


                                <div class="form-group ">
                                    <label for="" class="col-lg-3 control-label">Author</label>
                                    <div class="col-lg-9">

                                            <select name="select" class="selectbox form-control">
                                                <option value="#">Select Author</option>
                                                @foreach ($authors as  $author)
                                                    <option value="{{ $author->id }}">{{ $author->fname}}</option>
                                                @endforeach
                                            </select>
                                    <div style="color: red"> @error('select')
                                        {{$message}}
                                        @enderror
                                    </div>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Book Description:</label>
                                    <div class="col-lg-9">
                                        <textarea rows="5" cols="5" name="description" class="form-control" placeholder="Enter Description"></textarea>
                                    <div style="color: red"> @error('description')
                                        {{$message}}
                                        @enderror
                                    </div>
                                    </div>

                                </div>
                            </fieldset>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Add Book <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /basic legend -->

            </div>
        </div>




<!--
<form action="/book" method="POST">
    @csrf

    <div class="container mt-5">
        <h2>Insert Book Details</h2>
        <div class="form-group row mt-5 ">
            <label for="" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="title" placeholder="Title">
            </div>
            <div style="color: red"> @error('title')
                {{$message}}
                @enderror
            </div>
        </div>



        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Pages</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" name="pages" placeholder="Pages">
            </div>
            <div style="color: red"> @error('pages')
                {{$message}}
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Author</label>
            <div class="col-sm-4">

                    <select name="select" class="form-control">
                        <option value="#">Select Author</option>
                        @foreach ($authors as  $author)
                            <option value="{{ $author->id }}">{{ $author->fname}}</option>
                        @endforeach
                    </select>
            </div>
            <div style="color: red"> @error('select')
                {{$message}}
                @enderror
            </div>
        </div>



        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-4">
        <textarea name="description" class="form-control" id="" cols="30" rows="8" placeholder="Description"
        ></textarea>
            </div>
            <div style="color: red"> @error('description')
                {{$message}}
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <div class="col-sm-10 ">
                <button type="submit" class="btn btn-primary">Add Book</button>
            </div>
        </div>


    </div> -->

</form>
    @endsection

