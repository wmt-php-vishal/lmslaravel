@extends('html')
{{--@include('header')--}}

<form action="" method="POST" class="mt-5 container">
@csrf


        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" aria-describedby="" placeholder="Enter name">
            
        </div>

        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        </div>

        <button type="submit" class="btn btn-primary">Sign Up</button>
    </form>

    <form action="" class="mt-5 container">
        <button type="submit" class="btn btn-primary">Login Page</button>

    </form>
</form>
