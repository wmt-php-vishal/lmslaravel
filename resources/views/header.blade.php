<div class="navbar navbar-inverse  bg-indigo-400 navbar-component" style="position: relative; z-index: 24;">
    <div class="navbar-header ">
        <a class="navbar-brand  container" href="/lms">Lms Laravel</a>
        <ul class="nav navbar-nav  pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-buttons"><i class="icon-menu2"></i></a></li>
        </ul>
    </div>
    <div class="navbar-collapse  collapse" id="navbar-buttons">
        <div class="navbar-left ">
            <div class="collapse  text-white navbar-collapse" id="collapsibleNavbar">
                <div class="navbar-collapse  collapse" id="navbar-navigation">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Active link</a></li> -->
                        <li><a href="/lms">Home</a></li>
                        <li><a href="/lms">Author</a></li>
                        <li><a href="/book">Book</a></li>
                        <li><a href="#">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>