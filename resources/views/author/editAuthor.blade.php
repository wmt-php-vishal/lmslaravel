@extends('html')



<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">

@section('content')


<div class="row">
            <div class="col-md-8">

                <!-- Basic legend -->
                <form class="form-horizontal" action="/lms/{{$author->id}}" method="POST" id="editAuthor">
                @csrf
                @method('PUT')
                    <div class="panel panel-flat">
                        <div class="panel-heading">


                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <legend class="text-semibold">Update Author Details</legend>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">First Name:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="fname" value="{{$author->fname}}">
                                    <div style="color: red"> @error('fname')
                                        {{$message}}
                                        @enderror
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Last Name:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="lname" value="{{$author->lname}}">
                                    <div style="color: red"> @error('lname')
                                        {{$message}}
                                        @enderror
                                    </div>
                                    </div>
                                </div>

                                <div class="form-group">
										<label class="control-label col-lg-3">Date</label>
										<div class="col-lg-9">
                                            <input class="form-control" type="date" value="{{$author->dob}}" name="dob">
                                        <div style="color: red"> @error('dob')
                                            {{$message}}
                                            @enderror
                                        </div>
										</div>
									</div>

                                <!-- <div class="form-group ">
                                    <label class="col-lg-3 control-label">Date Of Birth</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control pickadate" name="dob" placeholder="Enter your Birthdate&hellip;">
                                    </div>
                                </div> -->

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Gender:</label>
                                    <div class="col-lg-9">
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" class="styled" value="Male" {{$author->gender == 'Male' ? "Checked" : "" }}>
                                            Male
                                        </label>

                                        <label class="radio-inline">
                                            <input type="radio" name="gender" class="styled" value="Female" {{$author->gender == 'Female' ? "Checked" : "" }}>Female</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Contact Number:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="mobile" value="{{$author->mobile}}" placeholder="Contact Number">
                                    <div style="color: red"> @error('mobile')
                                        {{$message}}
                                        @enderror
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Author Description:</label>
                                    <div class="col-lg-9">
                                        <textarea rows="5" cols="5" name="description" class="form-control">{{$author->author_description}}</textarea>
                                    <div style="color: red"> @error('description')
                                        {{$message}}
                                        @enderror
                                    </div>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Update Author <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /basic legend -->

            </div>
        </div>
<!-- old form -->
<!-- <form action="/lms/{{$author->id}}" method="POST" id="editAuthor">
    @csrf
    @method('PATCH')

    <div class="container mt-5">
        <h2>Update Author Details</h2>

        <div class="form-group row mt-5 ">
            <label for="" class="col-sm-2 col-form-label">First Name</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="fname" placeholder="First Name" value="{{$author->fname}}">
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Last Name</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="lname" placeholder="Last Name" value="{{$author->lname}}">
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">DOB</label>
            <div class="col-sm-4">
                <input type="date" class="form-control" name="dob" placeholder="Last Name" value="{{$author->dob}}">
            </div>
        </div>


        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Gender</label>
            <div class="col-sm-4">
                <input type="radio" name="gender" value="Male" {{$author->gender == 'Male' ? "Checked" : "" }}> Male <br>
                <input type="radio" name="gender" value="Female" {{$author->gender == 'Female' ? "Checked" : "" }}> Female <br>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Mobile</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="mobile" placeholder="Contact Number" value="{{$author->mobile}}">
            </div>
        </div>


        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-4">
        <textarea name="description" class="form-control" id="" cols="30" rows="8">{{$author->author_description}}</textarea>
            </div>
        </div>


        <div class="form-group row">
            <div class="col-sm-10 ">
                <button type="submit" class="btn btn-primary">Update Author</button>
            </div>
        </div>


    </div>

</form> -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    // just for the demos, avoids form submit
    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });

    $( "#editAuthor" ).validate({
        submitHandler: function(form) {

        rules: {
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            dob: {
                required: true
            },
            gender: {
                required: true
            },
            mobile: {
                required: true,
                digits: true,
                exactlength: 10,
            },
            description: {
                required: true,
            },
        }
        }
    });

</script>

@endsection
