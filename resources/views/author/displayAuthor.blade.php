@extends('html')

@section('title' , 'Display Author')
@section('js')
    <!-- Theme JS files -->

        <!-- <script type="text/javascript" src="assets/js/plugins/velocity/velocity.min.js"></script> -->
        <script type="text/javascript" src="assets/js/plugins/velocity/velocity.ui.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/buttons/spin.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/buttons/ladda.min.js"></script>
        <!-- <script type="text/javascript" src="assets/js/core/app.js"></script> -->
        <script type="text/javascript" src="assets/js/pages/components_buttons.js"></script>

        <script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /theme JS files -->
        <!-- Theme JS files -->
        <script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>

        <!-- <script type="text/javascript" src="assets/js/pages/datatables_basic.js"></script> -->
@endsection
@section('content')



    <!-- <form action="" method="GET"> -->
        <table class="table datatable-basic mt-5">
            <thead>
            <tr>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">DOB</th>
                <th scope="col">Gender</th>
                <th scope="col">Mobile</th>
                <th scope="col">Description</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($authors as $author)
                <tr>
                    <td>{{ $author->fname }}</td>
                    <td>{{ $author->lname }}</td>
                    <td>{{ $author->dob }}</td>
                    <td>{{ $author->gender }}</td>
                    <td>{{ $author->mobile }}</td>
                    <td>{{ $author->author_description }}</td>
                    <td class="text-center">

{{--                        <form action="/lms/{{$author->id}}" method="POST">--}}
{{--                            @csrf--}}
{{--                            @method('DELETE')--}}

<a href="/lms/{{$author->id}}/edit"><i class="fas fa-edit mx-2"></i></a>
<a href=""  class="border-0 ml-2 deleteBtn" data-id="{{ $author->id }}" name="delete"><i class="fas fa-trash  mx-2"> </i> </a>
{{--<button class="border-0 ml-2 deleteBtn" type="submit" data-id="{{ $author->id }}" name="delete"><i class="fas fa-trash-alt"></i></button>--}}
                    </td>
{{--    </form>--}}
    </tr>
    @endforeach
    </tbody>
    </table>

    <b> {{ $authors->count() . " Records Found"}}</b>

    <!-- Colored button -->

    <form action="/lms/create" class="text-center">
        <button type="submit" class=" btn btn-primary btn-lg text-center">Add New Author</button>
    </form>

    <!-- </form> -->
@endsection


<script src="{{asset('js/jquery.js')}}"></script>
<script type="text/javascript" >

    $(document).ready(function(){
        $(".deleteBtn").click(function(){
            // alert('hello');
            let authorId = $(this).attr('data-id');

            var url = "{{ route('lms.destroy' , ['lm' => ':lmId'])}}";
            url = url.replace(':lmId', authorId);
            console.log('------',url);
            console.log(authorId);
            $.ajax({
                type:'POST',
                data:{_method: 'delete',_token : '{{ csrf_token() }}' },
                url: url,

                success: function() {
                    window.location.href = "{{ route('lms.index')}}";


                }


            });
        });
    });
</script>
