@extends('html')

@section('js')
<!-- Theme JS files -->
<!-- <script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script> -->
<script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- <script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{asset('assets/js/pages/form_layouts.js')}}"></script> -->

<script type="text/javascript" src="{{asset('assets/js/plugins/ui/ripple.min.js')}}"></script>
{{--    <!-- /theme JS files -->--}}
<!-- Theme JS files -->
<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>


<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
<!-- /theme JS files -->

<!-- ajax  -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> -->
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@endsection
@section('title' , 'Add Author')
@section('content')


<div class="row">
    <div class="col-md-8">

        <!-- Basic legend -->
        <form class="form-horizontal" action="/lms" method="POST" id="addAuthor">
            @csrf
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Add New Author</h5>

                </div>

                <div class="panel-body">
                    <fieldset>


                        <div class="form-group">
                            <label class="col-lg-3 control-label">First Name:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="fname" placeholder="First Name">
                                <div style="color: red"> @error('fname')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Last Name:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="lname" placeholder="Last Name">
                                <div style="color: red"> @error('lname')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Date</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="date" name="dob">
                                <div style="color: red"> @error('dob')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>

                        </div>

                        <!-- <div class="form-group ">
                                    <label class="col-lg-3 control-label">Date Of Birth</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control pickadate" name="dob" placeholder="Enter your Birthdate&hellip;">
                                    </div>
                                </div> -->

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Gender:</label>
                            <div class="col-lg-9">
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="Male" class="styled" checked="checked">
                                    Male
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="Female" class="styled">Female</label>
                                <div style="color: red"> @error('gender')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Contact Number:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="mobile" placeholder="Contact Number">
                                <div style="color: red"> @error('mobile')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Author Description:</label>
                            <div class="col-lg-9">
                                <textarea rows="5" cols="5" name="description" class="form-control"
                                    placeholder="Enter your message here"></textarea>
                                <div style="color: red"> @error('description')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </fieldset>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Add Author <i
                                class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic legend -->

    </div>
</div>

<!-- 
        <form action="/lms" method="POST" id="addAuthor">
    @csrf

    <div class="container mt-5">
        <h2>Insert Author Details</h2>
        <div class="form-group row mt-5 ">
            <label for="fname" class="col-sm-2 col-form-label">First Name</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" email>
            </div>
            <div style="color: red"> @error('fname')
                {{$message}}
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="lname" class="col-sm-2 col-form-label">Last Name</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="lname" placeholder="Last Name">
            </div>
            <div style="color: red"> @error('lname')
                {{$message}}
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="dob" class="col-sm-2 col-form-label">DOB</label>
            <div class="col-sm-4">
                <input type="date" class="form-control" name="dob" placeholder="Last Name">
            </div>
            <div style="color: red"> @error('dob')
                {{$message}}
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <label for="gender" class="col-sm-2 col-form-label">Gender</label>
            <div class="col-sm-4">
                <input type="radio" name="gender"  value="Male"> Male
                <input type="radio" name="gender"  value="Female"> Female
            </div>
            <div style="color: red"> @error('gender')
                {{$message}}
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="mobile" class="col-sm-2 col-form-label">Mobile</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Contact Number">
            </div>
            <div style="color: red"> @error('mobile')
                {{$message}}
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-4">
        <textarea name="description" class="form-control" id="" cols="30" rows="8"
        ></textarea>
            </div>
            <div style="color: red"> @error('description')
                {{$message}}
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <div class="col-sm-10 ">
                <button type="submit" class="btn btn-primary">Add Author</button>
            </div>
        </div>


    </div>

</form>
<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

<script>
    $("#addAuthor").validate();
</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    // just for the demos, avoids form submit
    // jQuery.validator.setDefaults({
    //     debug: true,
    //     success: "valid"
    // });
    $( "#addAuthor" ).validate({
        rules: {
            fname: {
                required: true,
            },
            lname: {
                required: true
            },
            // dob: {
            //     required: true
            // },
            gender: {
                required: true
            },
            mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
                exact

            },
            description: {
                required: true,

            },

        }
    });

</script> -->

@endsection