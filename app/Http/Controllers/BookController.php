<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Http\Requests\Book\bookRequest;
use App\Http\Requests\Book\StoreBookRequest;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with('author')->get();
//        dd($books);
//        $book = Book::all();
//        echo $book->author->fname;
//        dd($books[0]->author->fname);


        return view('book/displayBook')->with('books' , $books);

//        return view('book/displayBook');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $authors = Author::all();

        return view('book/addBook')->with('authors' , $authors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(bookRequest $request)
    {
        $book = new Book;
      //  $validatedData = $request->validate([
       //     'title' => 'required|max:100',
       //     'pages' => 'required|numeric',
      //      'description' => 'required',
       // ],
       //     [
       //         'title.required' => ' Title is req',
       //         'title.max' => ' Title is req'

       //     ]);

        $book->title = $request->title;
        $book->pages = $request->pages;
        $book->author_id = $request->select;
        $book->book_description = $request->description;

        $book->save();

        if ($book->save())
        {
            // echo "Book Inserted Successfully";
           return redirect()->action('BookController@index');
        }
        else{
            echo "Insertion failed";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $book = Book::find($id);
//        $author = Book::find($id)->author;

        return view('book/showBook')->with('book' , $book);




//        $book = Author::find($id)->book;
//        echo count($book). " Records found <br>";
//        echo $book;

        $author = Book::find($id)->author;
        echo $author;
//        echo count($author). " Records found <br>";


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        $authors = Author::all();
        return view('book/editBook')->with('book' , $book)->with('authors' , $authors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(bookRequest $request, $id)
    {
        $book = Book::find($id);
        $book->title = $request->title;
        $book->pages = $request->pages;
        $book->book_description = $request->description;
        $book->author_id = $request->select;
        $book->save();

        return redirect()->action('BookController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        dd($request->all());
        $book = Book::find($id);
        $book->delete();

        return redirect()->action('BookController@create');


    }
}
