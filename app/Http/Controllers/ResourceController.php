<?php

namespace App\Http\Controllers;

use App\Http\Requests\Author\authorRequest;
use Illuminate\Http\Request;
use App\Author;
use App\Book;
use Illuminate\Support\Facades\Auth;


class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $authors = Author::all();
        return view('author/displayAuthor')->with('authors', $authors);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('author/addAuthor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(authorRequest $request)
    {
//        $validatedData = $request->validate([
//            'fname' => 'required|alpha|max:40',
//            'lname' => 'required|alpha|max:40',
//            'dob' => 'required ',
//            'gender' => 'required',
//            'mobile' => 'required|numeric|digits:10',
//            'description' => 'required',
//        ]);

        // dd($request->dob);

        $author = new Author;
        $author->fname = $request->fname;
        $author->lname = $request->lname;
        $author->dob = $request->dob;
        $author->gender = $request->gender;
        $author->mobile = $request->mobile;
        $author->author_description = $request->description;

        $author->save();
        $authors = Author::all();

        if ($author->save()) {
            return view('author/displayAuthor')->with('authors', $authors);
        } else {
            echo "data insertion failed";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = Author::find($id);

        return view('author/editAuthor')->with('author', $author);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(authorRequest $request, $id)
    {
//        $validatedData = $request->validate([
//            'fname' => 'required|alpha|max:40',
//            'lname' => 'required|alpha|max:40',
//            'dob' => 'required ',
//            'gender' => 'required',
//            'mobile' => 'required|numeric|digits:10',
//            'description' => 'required',
//        ]);

        $author = Author::find($id);

        $author->fname = $request->fname;
        $author->lname = $request->lname;
        $author->dob = $request->dob;
        $author->gender = $request->gender;
        $author->mobile = $request->mobile;
        $author->author_description = $request->description;

        $author->save();
        return redirect()->action('ResourceController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $author = Author::find($id);
        $author->delete();
        return back();

//        $authors = Author::all();
//        return view('author/displayAuthor')->with('authors', $authors);
//        Author::destroy($id);
//        return redirect()->route('lms/{id}');
//        return redirect()->action('ResourceController@index');
    }
}
