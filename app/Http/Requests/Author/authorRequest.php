<?php

namespace App\Http\Requests\Author;

use Illuminate\Foundation\Http\FormRequest;

class authorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()){
            case "POST":
                return [
                    'fname' => 'required|alpha|max:40',
            'lname' => 'required|alpha|max:40',
            'dob' => 'required ',
            'gender' => 'required',
            'mobile' => 'required|numeric|digits:10',
            'description' => 'required',
                ];
                break;
            case "PUT":
                return [
                    'fname' => 'required|alpha|max:41',
                    'lname' => 'required|alpha|max:40',
                    'dob' => 'required ',
                    'gender' => 'required',
                    'mobile' => 'required|numeric|digits:10',
                    'description' => 'required',
                ];
                break;
        }

    }
}
